require('codemirror/mode/clike/clike');
require('codemirror/addon/edit/closebrackets');
require('codemirror/addon/edit/matchbrackets');
require('codemirror/addon/comment/comment');
require('codemirror/addon/fold/foldcode');
require('codemirror/addon/fold/brace-fold');
require('codemirror/addon/fold/foldgutter');
require('codemirror/addon/dialog/dialog');
require('codemirror/addon/search/search');
require('codemirror/addon/search/jump-to-line');
require('codemirror/keymap/sublime');

const fs = require('fs');
const CodeMirror = require('codemirror');
const OJDigger = require('./oj_digger');

const { remote } = require('electron');
const dialog = remote.dialog;

/** @type {CodeMirror.Editor} */
let mir;

$('.menu .item')
    .tab();

function prepare_cm_editor() {
    if (document.querySelector('.CodeMirror'))
        return;

    let code_text_area = document.querySelector('#code');
    code_text_area.value = fs.readFileSync('template.c');

    mir = CodeMirror.fromTextArea(code_text_area, {
        lineNumbers: true,
        mode: 'text/x-c++src',
        theme: 'duotone-light',
        keyMap: 'sublime',
        tabSize: 4,
        indentUnit: 4,
        showCursorWhenSelecting: true,
        autoCloseBrackets: true,
        matchBrackets: true,
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
    });

    mir.setSize(null,500);
}

function on_template_tab_link() {
    prepare_cm_editor();
    document.title = 'Template editor - Digger';
}

function on_setup_tab_link() {
    document.title = 'Digger settings - Digger';
}

function on_output_tab_link() {
    document.title = 'Dig Output Logs - Digger';
}

function lock_down() {
    // lock form
    $('#setup-form').find('.field').each((idx, el) => $(el).toggleClass('disabled'));
    // lock button
    $('#start-button').toggleClass('disabled');
    $('#start-button').toggleClass('loading');
    // lock editor
    mir.setOption('readOnly', true);
    // show bar
    $('#progress-container').removeClass('hidden');

    // clear output
    let progress = $('.ui.progress');
    progress.progress('reset');
    $('#output-p').text('');
}

function un_lock_down() {
    // unlock form
    $('#setup-form').find('.field').each((idx, el) => $(el).toggleClass('disabled'));
    // unlock button
    $('#start-button').toggleClass('disabled');
    $('#start-button').toggleClass('loading');
    // unlock editor
    mir.setOption('readOnly', false);
}

async function on_start_dig() {
    let form_vals = $('#setup-form').form('get values');
    let template_str = mir.getValue();

    form_vals['start-bit'] = parseInt(form_vals['start-bit']);
    form_vals['end-bit'] = parseInt(form_vals['end-bit']);

    let done_count = 0;
    let all_count = form_vals['end-bit'] - form_vals['start-bit'] + 1;
    let batches = [];
    if (all_count > 24) {
        // 3 batches
        batches = [
            [form_vals['start-bit'], form_vals['start-bit'] + 11],
            [form_vals['start-bit'] + 12, form_vals['start-bit'] + 23],
            [form_vals['start-bit'] + 23, form_vals['end-bit']],
        ];
    } else if (all_count > 12) {
        batches = [
            [form_vals['start-bit'], form_vals['start-bit'] + 11],
            [form_vals['start-bit'] + 12, form_vals['end-bit']],
        ];
    } else {
        batches = [
            [form_vals['start-bit'], form_vals['end-bit' ]]
        ]
    }

    lock_down();

    // reset bottom status area
    let progress = $('.ui.progress');
    let progress_text = $('#progress-text');

    let cur_count = 0;

    let update_percentage = (done) => {
        let percentage = 10 + 85 * (cur_count / all_count);
        if (done)
            percentage = 100;
        progress.progress('set percent', percentage);
    };

    // event callbacks
    let on_login = () => {
        progress_text.text('Logged in');
        $('#output-p').append('<span>Logged in.</span><br>');
    };

    let on_submit = (str, prog, all) => {
        cur_count++;
        update_percentage();
        progress_text.text(`Submitting ${cur_count}/${all_count}...`);
        $('#output-p').append(`<span>Submitting ${cur_count}/${all_count}...</span><br>`);
    };

    let on_waiting = () => {
        progress_text.text('Waiting for judge...');
        $('#output-p').append(`<span>Waiting</span><br>`);
    };

    let on_stillwait = () => {
        $('#output-p').append(`<span>Still waiting...</span><br>`);
    };

    let on_all_done = () => {
        progress_text.text('Done. See output tab.');
        $('#output-p').append(`<span>Done</span><br>`);
        update_percentage(true);

        un_lock_down();
    };

    let on_batch_done = () => {
        progress_text.text('Current batch done.');
        $('#output-p').append(`<span>Batch done</span><br>`);
    }

    let concated_results;
    for (let batch of batches) {
        let start_bit = batch[0];
        let end_bit = batch[1];
        
        let digger = new OJDigger({
            username: form_vals['username'],
            password: form_vals['password'],
            pid: form_vals['problem-id'],
            lang: 'C',
            template: template_str,
        });

        digger.on('login', on_login);
        digger.on('submit', on_submit);
        digger.on('waiting', on_waiting);
        digger.on('stillwait', on_stillwait);
        digger.on('done', on_batch_done);

        await digger.start_digging(start_bit, end_bit, 1)
            .then(() => {
                return digger.retrieve_result(
                    start_bit,
                    end_bit,
                    1
                );
            })
            .then(all_results => {
                console.log(all_results);
                on_batch_done();
                if (!concated_results)
                    concated_results = all_results;
                else
                    for (let i = 0; i < all_results.length; i++)
                        concated_results[i] = all_results[i] + concated_results[i];

                $('#output-p').append(`<span style="color: #21ba45;">Results: ${concated_results}</span><br>`);
            });
    }

    $('#output-p').append(`<span style="color: #21ba45;">All done. Results:</span><br>`)
    for (let i = 0; i < concated_results.length; i++)
        $('#output-p')
            .append('<span style="color: #21ba45;">' +
                `${concated_results[i]}, ${parseInt(concated_results[i], 2)}` +
                '</span><br>');

    on_all_done();
    dialog.showMessageBox(
        remote.getCurrentWindow(), {
            title: 'Results',
            message: JSON.stringify(concated_results) + '\n'
            + JSON.stringify(concated_results.map(str => parseInt(str, 2))),
        },
    );
}

document.querySelector('#template-tab-link')
    .addEventListener('click', on_template_tab_link);

document.querySelector('#setup-tab-link')
    .addEventListener('click', on_setup_tab_link);

document.querySelector('#output-tab-link')
    .addEventListener('click', on_output_tab_link);

document.querySelector('#start-button')
    .addEventListener('click', on_start_dig);

$('document').ready(() => {
    prepare_cm_editor();
});
