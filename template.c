/**
 * templace.c
 * Example OJ digging template file.
 * Be sure to have only one occurence of $SHIFT
 *   for the digger to substitute with shift value.
 */

#include <stdio.h>
#include <stdlib.h>

// Don't touch this line
#define SHIFT $SHIFT

enum OJAction {
    RE, WA
};

// Get action for later use
enum OJAction get_oj_action(int input) {
    // Perform bitwise AND
    if (0b00000001 << SHIFT & input)
        return RE;
    else
        return WA;
}

// Perform the action
void do_oj_action(enum OJAction action) {
    switch (action) {
        case RE:
            exit(1);
        case WA:
            return;
    }
}

int main() {
    // Which input to get
    int nth_input = 1;

    // Get nth input
    int input;
    while (nth_input--)
        scanf("%d", &input);

    // Get action
    enum OJAction action = get_oj_action(input);
    // Do action
    do_oj_action(action);

    return 0;
}
